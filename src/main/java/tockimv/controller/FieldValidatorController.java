package tockimv.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class FieldValidatorController {

    @FXML
    Label message;

    public void setText(String message)
    {
        this.message.setText(message);
    }
}
