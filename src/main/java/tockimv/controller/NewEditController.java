package tockimv.controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import tockimv.model.Task;
import tockimv.services.DateService;
import tockimv.services.TasksService;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;


public class NewEditController {

    private static Button clickedButton;

    private static final Logger log = Logger.getLogger(NewEditController.class.getName());

    public static void setClickedButton(Button clickedButton) {
        NewEditController.clickedButton = clickedButton;
    }

    public static void setCurrentStage(Stage currentStage) {
        NewEditController.currentStage = currentStage;
    }

    private static Stage currentStage;

    private Task currentTask;
    private ObservableList<Task> tasksList;
    private TasksService service;
    private DateService dateService;


    private boolean incorrectInputMade;
    @FXML
    private TextField fieldTitle;
    @FXML
    private DatePicker datePickerStart;
    @FXML
    private TextField txtFieldTimeStart;
    @FXML
    private DatePicker datePickerEnd;
    @FXML
    private TextField txtFieldTimeEnd;
    @FXML
    private TextField fieldInterval;
    @FXML
    private CheckBox checkBoxActive;
    @FXML
    private CheckBox checkBoxRepeated;

    private static final String DEFAULT_START_TIME = "8:00";
    private static final String DEFAULT_END_TIME = "10:00";
    private static final String DEFAULT_INTERVAL_TIME = "0:30";

    public void setTasksList(ObservableList<Task> tasksList) {
        this.tasksList = tasksList;
    }

    public void setService(TasksService service) {
        this.service = service;
        this.dateService = new DateService(service);
    }

    public void setCurrentTask(Task task) {
        this.currentTask = task;
        switch (clickedButton.getId()) {
            case "btnNew":
                initNewWindow("New Task");
                break;
            default:
                initEditWindow("Edit Task");
                break;
        }
    }

    @FXML
    public void initialize() {
        log.info("new/edit window initializing");
    }

    private void initNewWindow(String title) {
        currentStage.setTitle(title);
        datePickerStart.setValue(LocalDate.now());
        txtFieldTimeStart.setText(DEFAULT_START_TIME);
    }

    private void initEditWindow(String title) {
        currentStage.setTitle(title);
        fieldTitle.setText(currentTask.getTitle());
        datePickerStart.setValue(DateService.getLocalDateValueFromDate(currentTask.getStartTime()));
        txtFieldTimeStart.setText(dateService.getTimeOfTheDayFromDate(currentTask.getStartTime()));

        if (currentTask.isRepeated()) {
            checkBoxRepeated.setSelected(true);
            hideRepeatedTaskModule(false);
            datePickerEnd.setValue(DateService.getLocalDateValueFromDate(currentTask.getEndTime()));
            fieldInterval.setText(service.getIntervalInHours(currentTask));
            txtFieldTimeEnd.setText(dateService.getTimeOfTheDayFromDate(currentTask.getEndTime()));
        }
        if (currentTask.isActive()) {
            checkBoxActive.setSelected(true);

        }
    }

    @FXML
    public void switchRepeatedCheckbox(ActionEvent actionEvent) {
        CheckBox source = (CheckBox) actionEvent.getSource();
        if (source.isSelected()) {
            hideRepeatedTaskModule(false);
        } else if (!source.isSelected()) {
            hideRepeatedTaskModule(true);
        }
    }

    private void hideRepeatedTaskModule(boolean toShow) {
        datePickerEnd.setDisable(toShow);
        fieldInterval.setDisable(toShow);
        txtFieldTimeEnd.setDisable(toShow);

        datePickerEnd.setValue(LocalDate.now());
        txtFieldTimeEnd.setText(DEFAULT_END_TIME);
        fieldInterval.setText(DEFAULT_INTERVAL_TIME);
    }

    @FXML
    public void saveChanges() {

        Task collectedFieldsTask = collectFieldsData();
        if (incorrectInputMade) return;
        if (currentTask == null) {//no task was chosen -> add button was pressed
            saveTask(collectedFieldsTask.getTitle(),collectedFieldsTask.getStartTime(),collectedFieldsTask.getEndTime(),collectedFieldsTask.getRepeatInterval(),collectedFieldsTask.isActive());
        } else {
            updateTask(collectedFieldsTask);
        }
        Controller.editNewStage.close();
    }

    public void saveTask(String descriere, Date dataInceput, Date dataSfarsit, int interval,boolean esteActiv){

        Task task;
        if(interval==0)
            task = new Task(descriere,dataInceput);
        else
            task = new Task(descriere,dataInceput,dataSfarsit,interval);

        task.setActive(esteActiv);
        tasksList.add(task);
        service.rewriteFile(tasksList);

    }

    public  int getTaskListSize(){

        if(tasksList==null)
            return 0;
        return tasksList.size();
    }

    public void updateTask(Task task){

        for (int i = 0; i < tasksList.size(); i++) {
            if (currentTask.equals(tasksList.get(i))) {
                tasksList.set(i, task);
            }
        }
        currentTask = null;
        service.rewriteFile(tasksList);
    }

    @FXML
    public void closeDialogWindow() {
        Controller.editNewStage.close();
    }

    private Task collectFieldsData() {
        incorrectInputMade = false;
        Task result = null;
        StringBuilder errors = new StringBuilder();
        result = makeTask(errors);
        if (errors.length() > 0) {

            incorrectInputMade = true;
            try {
                Stage stage = new Stage();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/field-validator.fxml"));
                Parent root = loader.load();
                FieldValidatorController fieldValidatorController = loader.getController();
                fieldValidatorController.setText(errors.toString());
                stage.setScene(new Scene(root, 350, 150));
                stage.setResizable(false);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException ioe) {
                log.error("error loading field-validator.fxml");
            }
        }
        return result;
    }

    private Task makeTask(StringBuilder errors) {
        Task result = new Task();
        Date newStartDate = null;
        String newTitle = fieldTitle.getText();
        if (newTitle.isEmpty()) errors.append("Invalid title.\n");
        try {
            Date startDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerStart.getValue());//ONLY date!!without time
            newStartDate = dateService.getDateMergedWithTime(txtFieldTimeStart.getText(), startDateWithNoTime);
            if (checkBoxRepeated.isSelected()) {
                try {
                    Date endDateWithNoTime = dateService.getDateValueFromLocalDate(datePickerEnd.getValue());
                    Date newEndDate = dateService.getDateMergedWithTime(txtFieldTimeEnd.getText(), endDateWithNoTime);
                    int newInterval = service.parseFromStringToSeconds(fieldInterval.getText());
                    if (newStartDate.after(newEndDate))
                        errors.append("Start date should be before end.\n");
                    result = new Task(newTitle, newStartDate, newEndDate, newInterval);
                } catch (RuntimeException e) {
                    errors.append("Invalid EndDate.\n");
                }
            } else {
                result = new Task(newTitle, newStartDate);
            }
            boolean isActive = checkBoxActive.isSelected();
            result.setActive(isActive);
            log.info(result);
        } catch (RuntimeException e) {
            errors.append("Invalid StartDate.\n");
        }


        return result;
    }


}
