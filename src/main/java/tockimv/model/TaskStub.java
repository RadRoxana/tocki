package tockimv.model;

import org.apache.log4j.Logger;
import tockimv.repository.TaskIO;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TaskStub extends Task {
    private String title;
    private Date start;
    private Date end;
    private int interval;
    private boolean active;

    private static final Logger log = Logger.getLogger(TaskStub.class.getName());
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");


    public SimpleDateFormat getFormat(){return sdf;}
    public static SimpleDateFormat getDateFormat(){
         TaskStub task = new TaskStub();
         return task.getFormat(); }

    public TaskStub(){

        init();
    }
    private void init(){
        this.title = "stub task";
        this.start = new Date("2020/3/22");
        this.end = new Date("2020/3/23");
        this.interval = 5000;
        this.active = true;
    }
    public TaskStub(String title, Date time){
        init();

    }
    public TaskStub(String title, Date start, Date end, int interval){
        init();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public boolean isActive(){
        return this.active;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public Date getTime() {
        return start;
    }

    public void setTime(Date time) {
        this.start = time;
        this.end = time;
        this.interval = 0;
    }

    public Date getStartTime() {
        return getTime();
    }

    public Date getEndTime() {
        return end;
    }
    public int getRepeatInterval(){
        return interval > 0 ? interval : 0;
    }

    public void setTime(Date start, Date end, int interval){
        this.start = start;
        this.end = end;
        this.interval = interval;

    }
    public boolean isRepeated(){
        return this.interval != 0;

    }
    public Date nextTimeAfter(Date current){
        if (current.after(end) || current.equals(end))return null;
        if (isRepeated() && isActive()){
            Date timeBefore  = start;
            Date timeAfter = start;
            if (current.before(start)){
                return start;
            }
            Date rez=null;
            if ((current.after(start) || current.equals(start)) && (current.before(end) || current.equals(end)))  rez=  nextTimeAfterhelp(current,timeAfter,timeBefore);
            if(rez!=null) return rez;
        }
        if (!isRepeated() && current.before(start) && isActive()){
            return start;
        }
        return null;
    }


    private Date nextTimeAfterhelp(Date current,Date timeAfter,Date timeBefore) {
        for (long i = start.getTime(); i <= end.getTime(); i += interval * 1000) {
            if (current.equals(timeAfter)) return new Date(timeAfter.getTime() + interval * 1000);
            if (current.after(timeBefore) && current.before(timeAfter)) return timeBefore;//return timeAfter
            timeBefore = timeAfter;
            timeAfter = new Date(timeAfter.getTime() + interval * 1000);
        }
        return null;
    }
    //duplicate methods for TableView which sets column
    // value by single method and doesn't allow passing parameters
    public String getFormattedDateStart(){
        return sdf.format(start);
    }
    public String getFormattedDateEnd(){
        return sdf.format(end);
    }
    public String getFormattedRepeated(){
        if (isRepeated()){
            String formattedInterval = TaskIO.getFormattedInterval(interval);
            return "Every " + formattedInterval;
        }
        else {
            return "No";
        }
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskStub task = (TaskStub) o;

        if (!start.equals(task.start)) return false;
        if (!end.equals(task.end)) return false;
        if (interval != task.interval) return false;
        if (active != task.active) return false;
        return title.equals(task.title);
    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + start.hashCode();
        result = 31 * result + end.hashCode();
        result = 31 * result + interval;
        result = 31 * result + (active ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", interval=" + interval +
                ", active=" + active +
                '}';
    }

    @Override
    protected TaskStub clone() throws CloneNotSupportedException {
        TaskStub task  = (TaskStub)super.clone();
        task.start = (Date)this.start.clone();
        task.end = (Date)this.end.clone();
        return task;
    }
}


