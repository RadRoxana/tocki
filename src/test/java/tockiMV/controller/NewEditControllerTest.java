package tockiMV.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import tockimv.controller.NewEditController;
import tockimv.model.ArrayTaskList;
import tockimv.model.Task;
import tockimv.services.TasksService;
import tockimv.view.Main;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class NewEditControllerTest {


    NewEditController newEditController;
    static SimpleDateFormat sdf;
    static ClassLoader classLoader;


    @BeforeAll
    @DisplayName("setup")
    void setup() throws IOException {

        newEditController = new NewEditController();
        ArrayList<Task> tasks = new ArrayList<>();
        ObservableList<Task> observableList = FXCollections.observableArrayList(tasks);
        newEditController.setTasksList(observableList);


        TasksService tasksService = new TasksService(new ArrayTaskList());
        newEditController.setService(tasksService);

        sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

        classLoader = Main.class.getClassLoader();
    }


    @Test
    @Order(1)
    public void TC03_BVA() {

        try {
            int listSizeBefore;
            int listSizeAfter;

            listSizeBefore = newEditController.getTaskListSize();

            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            newEditController.saveTask("Descriere", dateStar, dateEnd, 1, true);

            listSizeAfter = newEditController.getTaskListSize();
            assertNotEquals(listSizeBefore, listSizeAfter);

        } catch (RuntimeException | ParseException ex) {
            assert (false);
        }
    }

    @Test
    @Order(2)
    public void TC08_BVA() {

        try {
            int listSizeBefore;
            int listSizeAfter;

            listSizeBefore = newEditController.getTaskListSize();

            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            newEditController.saveTask("Descriere", dateStar, dateEnd, 10, false);

            listSizeAfter = newEditController.getTaskListSize();
            assertNotEquals(listSizeBefore, listSizeAfter);

        } catch (RuntimeException | ParseException ex) {
            assert (false);
        }
    }

    @Test
    @Order(3)
    public void TC09_BVA() {

        try {
            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            Boolean esteActiv = null;
            newEditController.saveTask("Descriere", dateStar, dateEnd, 10, esteActiv);
            assert (false);
        } catch (RuntimeException | ParseException ex) {
            assert (true);
        }

    }

    @Test
    @Order(4)
    public void TC01_BVA() {

        try {
            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            newEditController.saveTask("Descriere", dateStar, dateEnd, -1, true);
            assert (false);
        } catch (RuntimeException | ParseException ex) {
            assert (true);
        }
    }

    @Test
    @Order(5)
    public void TC1_ECP() {

        try {
            int listSizeBefore;
            int listSizeAfter;

            listSizeBefore = newEditController.getTaskListSize();

            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            newEditController.saveTask("Descriere", dateStar, dateEnd, 10, true);

            listSizeAfter = newEditController.getTaskListSize();
            assertNotEquals(listSizeBefore, listSizeAfter);
            assert (true);
        } catch (RuntimeException | ParseException ex) {
            assert (false);
        }
    }

    @ParameterizedTest
    @CsvSource({
            "-10,         false"
    })
    public void TC2_ECP(int interval, boolean esteACtiv) {

        try {
            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            newEditController.saveTask("Descriere", dateStar, dateEnd, interval, esteACtiv);
            assert (false);
        } catch (RuntimeException | ParseException ex) {
            assert (true);
        }
    }


    @ParameterizedTest
    @ValueSource(ints = {10, 10})
    public void TC3_ECP(int interval) {

        try {
            String dateInString1 = "25-03-2020 17:05:203";
            String dateInString2 = "25-03-2020 20:05:203";
            Date dateStar = sdf.parse(dateInString1);
            Date dateEnd = sdf.parse(dateInString2);
            Boolean esteActiv = null;
            newEditController.saveTask("Descriere", dateStar, dateEnd, interval, esteActiv);
            assert (false);
        } catch (RuntimeException | ParseException ex) {
            assert (true);
        }
    }

}