package tockiMV.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tockimv.model.ArrayTaskList;
import tockimv.model.TaskStub;
import tockimv.services.TasksService;

public class IntegrateR {

    ArrayTaskList arrayTaskList;
    TasksService tasksService;

    @BeforeEach
    void setUp(){

        arrayTaskList = new ArrayTaskList();
        arrayTaskList.add(new TaskStub());
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    public void integrateR_getObservableListTest(){

        assert(arrayTaskList.getAll().size() == 1);
    }

    @Test
    public void integrateR_getIntervalInHoursTest(){

        assert(tasksService.getIntervalInHours(tasksService.getObservableList().get(0)).equals("01:23"));
    }

}