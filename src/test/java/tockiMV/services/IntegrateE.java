package tockiMV.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tockimv.model.ArrayTaskList;
import tockimv.model.Task;
import tockimv.model.TaskStub;
import tockimv.services.TasksService;

import java.util.Date;

public class IntegrateE {

    ArrayTaskList arrayTaskList;
    TasksService tasksService;

    @BeforeEach
    void setUp(){

        arrayTaskList = new ArrayTaskList();
        Task task = new Task("task",new Date("2020/3/22"),new Date("2020/3/22"),5000);
        task.setActive(true);
        arrayTaskList.add(task);
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    public void integrateE_getObservableListTest(){

        assert(arrayTaskList.getAll().size() == 1);
    }

    @Test
    public void integrateE_getIntervalInHoursTest(){

        assert(tasksService.getIntervalInHours(tasksService.getObservableList().get(0)).equals("01:23"));
    }

}