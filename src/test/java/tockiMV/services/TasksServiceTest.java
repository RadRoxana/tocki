package tockiMV.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tockimv.model.ArrayTaskList;
import tockimv.model.Task;
import tockimv.services.TasksService;
import java.util.Arrays;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class TasksServiceTest {


    ArrayTaskList arrayTaskList;
    TasksService tasksService;

    @BeforeEach
    void setUp() {

        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    public void getObservableListTest() {

        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(mock(Task.class), mock(Task.class), mock(Task.class)));
        Mockito.when(arrayTaskList.size()).thenReturn(3);
        assertEquals(tasksService.getObservableList().size(), 3);
    }

    @Test
    public void getIntervalInHoursTest() {

        Mockito.when(arrayTaskList.getAll()).thenReturn(Arrays.asList(mock(Task.class)));
        assertEquals(tasksService.getIntervalInHours(tasksService.getObservableList().get(0)),"00:00");
    }

}