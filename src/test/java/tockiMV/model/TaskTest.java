package tockiMV.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tockimv.model.Task;

import java.util.Date;

class TaskTest {

    Task task;

    @BeforeEach
    void setUp() {
        task = new Task("test", new Date("2020/2/1"), new Date("2020/2/2"), 5);
        task.setActive(true);
    }

    @Test
    void nextTimeAfterDateAfterEnd() {

        assert (task.nextTimeAfter(new Date("2020/6/1")) == null);
    }

    @Test
    void nextTimeAfterDateBeforeStart() {

        assert (task.nextTimeAfter(new Date("2020/1/1")).equals(task.getStartTime()));
    }
}