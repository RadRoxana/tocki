package tockiMV.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tockimv.model.ArrayTaskList;
import tockimv.model.Task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class ArrayTaskListTest {

    ArrayTaskList arrayList;

    @BeforeEach
    void setUp() {
        arrayList = new ArrayTaskList();
    }

    @Test
    public void saveTaskTest() {

        Task task = mock(Task.class);
        arrayList.add(task);
        assertEquals(arrayList.size(), 1);
    }

    @Test
    public void getAllTest() {

        Task task1 = mock(Task.class);
        Task task2 = mock(Task.class);
        Task task3 = mock(Task.class);

        arrayList.add(task1);
        arrayList.add(task2);
        arrayList.add(task3);

        assertEquals(arrayList.getAll().size(), 3);
    }
}