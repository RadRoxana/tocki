package tockiMV.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import tockimv.model.Task;
import tockimv.model.TasksOperations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TasksOperationsTest {

    private static Task task1;
    private static Task task2;
    private static SimpleDateFormat sdf;
    private static TasksOperations tasksOps;
    private static ObservableList<Task> observableList;

    @BeforeAll
    @DisplayName("setup")
    static void setup() throws ParseException {

        sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        String dateInString1 = "01-04-2020 10:10";
        String dateInString2 = "01-04-2020 12:10";
        Date dateStar = sdf.parse(dateInString1);
        Date dateEnd = sdf.parse(dateInString2);
        task1 = new Task("Descriere", dateStar, dateEnd, 2);
        task1.setActive(true);


        dateInString1 = "01-04-2020 10:20";
        dateInString2 = "01-04-2020 12:20";
        dateStar = sdf.parse(dateInString1);
        dateEnd = sdf.parse(dateInString2);
        task2 = new Task("Descriere", dateStar, dateEnd, 2);
        task2.setActive(true);


    }



    @ParameterizedTest
    @CsvSource({
            "02-04-2020 13:10,         null"
    })
    public void F02_TC1(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            tasksOps.incoming(dateStar, dateEnd);
            assert (false);
        } catch (RuntimeException ex) {
            assert (true);
        }

    }


    @ParameterizedTest
    @CsvSource({
            "null,         02-04-2020 13:10"
    })
    public void F02_TC2(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            tasksOps.incoming(dateStar, dateEnd);
            assert (false);
        } catch (RuntimeException ex) {
            assert (true);
        }


    }



    @ParameterizedTest
    @CsvSource({
            "02-04-2020 13:10,         02-04-20 13:20"
    })
    public void F02_TC3(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        observableList.add(task1);
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            Iterable<Task> list = tasksOps.incoming(dateStar, dateEnd);
            assertEquals(list.spliterator().getExactSizeIfKnown(), 0);
        } catch (RuntimeException ex) {
            assert (false);
        }
    }


    @ParameterizedTest
    @CsvSource({
            "01-04-2020 8:00,         01-04-2020 9:00"
    })
    public void F02_TC4(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        observableList.add(task1);
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            Iterable<Task> list = tasksOps.incoming(dateStar, dateEnd);
            assertEquals(list.spliterator().getExactSizeIfKnown(), 0);
        } catch (RuntimeException ex) {
            assert (false);
        }

    }

    @ParameterizedTest
    @CsvSource({
            "01-04-2020 10:00,         01-04-2020 12:10"
    })
    public void F02_TC6(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        observableList.add(task1);
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            Iterable<Task> list = tasksOps.incoming(dateStar, dateEnd);
            assertEquals(list.spliterator().getExactSizeIfKnown(), 1);
        } catch (RuntimeException ex) {
            assert (false);
        }
    }


    @ParameterizedTest
    @CsvSource({
            "01-04-2020 10:00,         01-04-2020 15:00"
    })
    public void F02_TC5(String startTime, String endTime) throws ParseException {

        observableList = FXCollections.observableArrayList();
        observableList.add(task1);
        observableList.add(task2);
        tasksOps = new TasksOperations(observableList);

        String dateInString1 = startTime;
        String dateInString2 = endTime;
        Date dateStar;
        Date dateEnd;
        dateStar = (startTime.equals("null")) ? null : sdf.parse(dateInString1);
        dateEnd = (endTime.equals("null")) ? null : sdf.parse(dateInString2);

        try {
            Iterable<Task> list = tasksOps.incoming(dateStar, dateEnd);
            assertEquals(list.spliterator().getExactSizeIfKnown(), 2);
        } catch (RuntimeException ex) {
            assert (false);
        }

    }
}